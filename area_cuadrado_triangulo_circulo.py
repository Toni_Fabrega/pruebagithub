class Coordenada:
    def __init__(self,x,y,z):
        self.__x = x
        self.__y = y
        self.__z = z
    def GetX(self):
        return self.__x
    def GetY(self):
        return self.__y
    def GetZ(self):
        return self.__z
    def SetX(self,x):
        self.__x = x
    def SetY(self,y):
        self.__y = y
    def SetZ(self,z):
        self.__z = z

class Cuadrado:
    def __init__(self, cAltura):
        self.__altura =  cAltura


    def calculo_area_cuadrado(self):
        return self.__altura*self.__altura



####


class Triangulo:
    def __init__(self, tBase, tAltura):
        self.__base = tBase
        self.__altura = tAltura


    def calculo_area_triangulo(self):
        return ( (self.__base*self.__altura)/2 )
        



####

from math import pi


class Circulo:
    def __init__(self, cRadio):
        self.__radio = cRadio
        

    def calculo_area_circulo(self):
        return ( pi*(self.__radio*self.__radio) )
        


class Trapecio:
    def __init__(self, Base1, Base2, Altura):
        self.__baseA = Base1
        self.__baseB = Base2
        self.__alt = Altura
